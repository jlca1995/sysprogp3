
#include <stdio.h>
#include <string.h>


int main()
{
	char c[25];
	char nombre[10][25];
	char apellido[10][25];
	int edad[10];
	int n;
	int b = 0;
	int viejo=0;
	int joven=0;
	float prom=0;
    printf("Práctica Programación de Sistemas \n\n");

    for (int i = 0; i < 10; ++i)
    {
    	b=0;
    	while (b==0)
    	{
    		printf("Ingrese su nombre:\t");
    		scanf("%s", c);
    		for (int j = 0; j < strlen(c); ++j)
    		{
    			b=0;
    			if(j==0 && ((int)c[j]<65 || (int)c[j]>90))
    			{
    				break;
    			}
    			else if(j!=0 && ((int)c[j]<97 || (int)c[j]>122))
    			{
    				break;
    			}
    			else
    			{
    				b=1;
    			};

    		}
    		if (b==0)
    		{
    			printf("Nombre ingresado es incorrecto\n");
    		}
    	}
    	strcpy(nombre[i], c);
    	b=0;
    	while (b==0)
    	{
    		printf("Ingrese su apellido:\t");
    		scanf("%s", c);
    		for (int j = 0; j < strlen(c); ++j)
    		{
    			b=0;
    			if(j==0 && ((int)c[j]<65 || (int)c[j]>90))
    			{
    				break;
    			}
    			else if(j!=0 && ((int)c[j]<97 || (int)c[j]>122))
    			{
    				break;
    			}
    			else
    			{
    				b=1;
    			};

    		}
    		if (b==0)
    		{
    			printf("Apellido ingresado es incorrecto\n");
    		}
    	}
    	strcpy(apellido[i], c);
    	b=0;
    	while (b==0)
    	{
    		printf("Ingrese su edad:\t");
    		scanf("%d", &n);
    		if (n>0 && n<130)
    		{
    			edad[i]=n;
    			b=1;
    		}
    		else
    			printf("Edad incorrecta\n");
    	}
    }
    for (int i = 0; i < 10; ++i)
    {
    	prom += edad[i];
    	if (edad[i]>= edad[viejo])
    		viejo = i;
    	if (edad[i] <= edad[joven])
    		joven = i;
    }
    prom = prom/10;

     printf("Promedio de edad:\t %f \n",prom);
     printf("Persona más joven:\t %s %s, edad:  %d \n",nombre[joven], apellido[joven], edad[joven]);
     printf("Persona más vieja:\t %s %s, edad:  %d \n",nombre[viejo], apellido[viejo], edad[viejo]);
 

    return 0;
}